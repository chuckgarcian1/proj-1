`timescale 1ns / 1ps
`default_nettype none
module barrelshifter_tb #(parameter int D_SIZE = 4) ();
  logic                           rst_in;
  logic [D_SIZE-1:0]              x_in;
  logic [$clog2(D_SIZE)-1:0]      b_in;
  logic [2:0]                     op_in;
  logic [D_SIZE-1:0]              y_out;
  logic [D_SIZE-1:0]              y_out_exp;
  logic                           zf_out;
  logic                           vf_out;
  
  barrelshifter_comb_structural  #(.D_SIZE(D_SIZE)) dut (
    .x_in(x_in),
    .s_in(b_in),
    .op_in(op_in),
    .y_out(y_out),
    .zf_out(zf_out),
    .vf_out
  );
  
  initial begin
    $display("Starting Sim"); //print nice message at start
    $display("x_in    s_in    op_in   c_out   zf_out  vf_out");
    for (int op = 0; op < 8; op++) begin 
      for (int i = 0; i < $pow(2, D_SIZE); i++) begin
        for (int j = 0; j < D_SIZE; j++) begin
          
          rst_in = 1; #10
          x_in = i;
          b_in = j;
          op_in = op;
          #10;
          
          $display("%b\t%b\t%3b\t%b\t%1b\t%1b",x_in, b_in, op_in, y_out, zf_out, vf_out); //print values C-style formatting
        end
      end
    end
        $display("Finishing Sim"); //print nice message at end
        $finish;
    end
endmodule: barrelshifter_tb
`default_nettype wire