// 2-to-1 Multiplexer Module
module mux2to1 (
  input in0,
  input in1,
  input sel,
  output logic out
  );
    
    always_comb begin
        casex (sel)
            1'b0: out = in0; 
            1'b1: out = in1;
        endcase
    end
endmodule

module overflow_flag #(parameter int D_SIZE = 8, parameter int STAGE_NUM = 0) (
    input wire[D_SIZE-1:0] stg_in,
    input wire lsb_x,
    input wire shift_select,
    output wire overflow_flag
);
    wire[2**STAGE_NUM] result_xor;
    genvar i;
    generate
      for (i = 1; i <= 2**STAGE_NUM; i = i + 1) begin
        wire w1;
        mux2to1 mux_lower_upper (
          .in0(lsb_x), 
          .in1(stg_in[i]),
          .sel(shift_select),
          .out(w1)
        );
        assign result_xor[i-1] = w1 ^ lsb_x;
      end    
    endgenerate
    assign overflow_flag = |result_xor;
endmodule

//  Stage 'k' Module
module stage_barrelshifter #(parameter int D_SIZE = 8, parameter int STAGE_NUM = 0) (
    input wire[D_SIZE-1:0] x_in,
    input wire shift_select,
    input wire[2**STAGE_NUM - 1: 0] shift_in,
    output logic[D_SIZE-1:0] y_out
);
    genvar i;
    generate
        for (i = 0; i < D_SIZE; i = i + 1) begin
            if (i < D_SIZE - 2**STAGE_NUM) begin
              mux2to1 mux_lower_upper (
                .in0(x_in[i]), 
                .in1(x_in[i + 2**STAGE_NUM]), 
                .sel(shift_select),
                .out(y_out[i])
              );   
            end else begin
                mux2to1 mux_input_shift (
                    .in0(x_in[i]), 
                    .in1(shift_in[i % 2**STAGE_NUM]), 
                    .sel(shift_select), 
                    .out(y_out[i])
                );          
            end 
        end    
    endgenerate
endmodule: stage_barrelshifter

// Data Reverse Mux Module
module data_rev_mux #(parameter int D_SIZE = 8) (
    input [D_SIZE - 1:0] rev_input,
    input wire sel,
    output [D_SIZE - 1:0] rev_output
);
    genvar rev_index;
    generate
        for (rev_index = 0; rev_index < D_SIZE; rev_index = rev_index + 1) begin
            mux2to1 mux_data_reverse (
                .in0(rev_input[rev_index]),
                .in1(rev_input[D_SIZE - rev_index - 1]),
                .sel(sel),
                .out(rev_output[rev_index])
            );
        end
    endgenerate
endmodule: data_rev_mux

// Return y0_out as x_in if the operation is lsa, otherwise let y0_in propogate
module sla_msb (
  input x0_in,
  input y0_in,
  input wire[2:0] op_in,
  output logic y0_out
  );
  wire w1;
  
  localparam LEFT_OP = 2;
  localparam ROTATE_OP = 1;
  localparam ARITH_OP = 0;
  
  // Sets w1 to zero if op is not a left operation, if it is then w1 is set
  // to if op is an arithmetic operation  
  mux2to1 left_op_selector (
    .in0(1'b0),
    .in1(op_in[ARITH_OP] & (~op_in[ROTATE_OP])),
    .sel(op_in[LEFT_OP]),
    .out(w1)
  );

  // Selects the input for y0_out based on the operation type
  mux2to1 y0_input_selector (
    .in0(y0_in),
    .in1(x0_in),
    .sel(w1),
    .out(y0_out)
  );
endmodule: sla_msb

// module barrelshifter_comb_structural 
//     ( input wire rst_in,
//       input wire [D_SIZE-1:0] x_in,
//       input wire [$clog2(D_SIZE)-1:0] s_in,
//       input wire [2:0] op_in,
//       output logic [D_SIZE-1:0] y_out,
//       output logic zf_out,
//       output logic vf_out );
// Main Barrelshiter Module

module barrelshifter_comb_structural # (parameter int D_SIZE = 8) (
    input wire                  rst_in,
    input wire [D_SIZE-1:0]  x_in,
    input wire [$clog2(D_SIZE)-1:0]  s_in,
    input wire [2:0]            op_in,
    output logic [D_SIZE-1:0]   y_out,
    output logic                zf_out,
    output logic                vf_out
);
    localparam LEFT_OP = 2;
    localparam LSR_OP = 0;
    localparam NUM_STAGES = $clog2(D_SIZE);
    // Reverse Input Data if Necessary
    logic [D_SIZE-1:0] rev_x;
    data_rev_mux #(.D_SIZE(D_SIZE)) initial_data_rev_mux_inst (
      .rev_input(x_in),
      .sel(op_in[2]),
      .rev_output(rev_x)
    );
    wire w1;
    mux2to1 left_op_selector (
      .in0(op_in[LSR_OP]),
      .in1(1'b0),
      .sel(op_in[LEFT_OP]),
      .out(w1)
    );
    // Select S for arithmetic operations
    wire s_wire;
    mux2to1 mx0 (
        .in0(1'b0),
        .in1(x_in[D_SIZE - 1]),
        .sel(w1),
        .out(s_wire)
    );
    
    wire [NUM_STAGES-1:0] overflow;
    genvar i;
    
    generate
        logic [D_SIZE-1:0] tmp_y [0:NUM_STAGES - 1];
        
        // Iterate through each stage
        for (i = NUM_STAGES - 1; i >= 0; i = i - 1) begin : gen_stage
            wire[$pow(2,i)-1:0] s_out;
            genvar j;

            // Generate mux row for rotate operations
            for (j = 0; j < 2**i; j = j + 1) begin
                if (i == NUM_STAGES - 1) begin
                    mux2to1 mx33 (
                        .in0(s_wire),
                        .in1(rev_x[j]),
                        .sel(op_in[1]),
                        .out(s_out[j])
                    );
                end else begin
                    mux2to1 mx33 (
                        .in0(s_wire),
                        .in1(tmp_y[i+1][j]),
                        .sel(op_in[1]),
                        .out(s_out[j])
                    );
                end
            end

            if (i == 0) begin: last_stage
                stage_barrelshifter #(.D_SIZE(D_SIZE), .STAGE_NUM(i)) final_stage_barrelshifter_inst (
                    .x_in(tmp_y[i+1]),
                    .shift_select(s_in[i]),
                    .shift_in(s_out),
                    .y_out(tmp_y[i])
                );

                overflow_flag #(.D_SIZE(D_SIZE), .STAGE_NUM(i)) ov1 (
                  .stg_in(tmp_y[i+1]),
                  .lsb_x(rev_x[0]),
                  .shift_select(s_in[i]),
                  .overflow_flag(overflow[i])
                );

            end
            else if (i == NUM_STAGES - 1) begin: first_stage
                stage_barrelshifter #(.D_SIZE(D_SIZE), .STAGE_NUM(i)) first_stage_barrelshifter_inst (
                  .x_in(rev_x),
                  .shift_select(s_in[i]),
                  .shift_in(s_out),
                  .y_out(tmp_y[i])
                );
                overflow_flag #(.D_SIZE(D_SIZE), .STAGE_NUM(i)) ov0 (
                  .stg_in(rev_x),
                  .lsb_x(rev_x[0]),
                  .shift_select(s_in[i]),
                  .overflow_flag(overflow[i])
                );
            end else begin: intermediate_stage
                stage_barrelshifter #(.D_SIZE(D_SIZE), .STAGE_NUM(i)) intermediate_stage_barrelshifter_inst (
                    .x_in(tmp_y[i+1]),
                    .shift_select(s_in[i]),
                    .shift_in(s_out),
                    .y_out(tmp_y[i])
                );

                overflow_flag #(.D_SIZE(D_SIZE), .STAGE_NUM(i)) ov2 (
                  .stg_in(tmp_y[i+1]),
                  .lsb_x(rev_x[0]),
                  .shift_select(s_in[i]),
                  .overflow_flag(overflow[i])
                );
            end
        end
    endgenerate

    logic [D_SIZE-1:0] rev_y;
    data_rev_mux #(.D_SIZE(D_SIZE)) final_data_rev_mux_inst (
      .rev_input(tmp_y[0]),
      .sel(op_in[LEFT_OP]),
      .rev_output(rev_y)
    );
    wire msb_output;
    sla_msb sla (
      .x0_in(x_in[D_SIZE-1]),
      .y0_in(rev_y[D_SIZE - 1]),
      .op_in(op_in),
      .y0_out(msb_output)
    );
    
    
     assign y_out = {msb_output, rev_y[D_SIZE - 2: 0]};
     assign vf_out = (|overflow) & (op_in[2] & op_in[0] & ~op_in[1]);
     assign zf_out = &(~({msb_output, rev_y[D_SIZE - 2: 0]}));
    
    

endmodule